# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM nvidia/cudagl:11.3.0-devel-ubuntu20.04

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="July 11, 2023"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo


RUN apt-get update && apt-get install -q -y --no-install-recommends \
    wget build-essential gcc git vim \
    dirmngr vim sudo git\
    libosmesa6-dev wget iputils-ping net-tools \
    x11-utils x11-apps terminator xterm \
    lsb-release iproute2 gnupg gnupg2 gnupg1 \
    ca-certificates language-pack-ja-base locales fonts-takao \
    debconf-utils curl  python3-pip wget && \
    pip install -U --no-cache-dir supervisor supervisor_twiddler rosdep && \
    rm -rf /var/lib/apt/lists/*


# Install ROS Noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt-get update \
 && apt-get install -y --no-install-recommends ros-noetic-desktop-full
RUN apt-get install -y --no-install-recommends python3-rosdep
RUN rosdep init \
 && rosdep fix-permissions \
 && rosdep update


RUN source /opt/ros/noetic/setup.bash && \
    mkdir -p /tmp/catkin_ws/src && cd /tmp/catkin_ws/src && \
    catkin_init_workspace && \
    git clone https://github.com/rt-net/sciurus17_ros.git && \
    git clone https://github.com/rt-net/sciurus17_description.git && \
    cd /tmp/catkin_ws && \
    rosdep update && apt-get update && \
    rosdep install --from-paths src --ignore-src -r -y && \
    catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/ros/noetic install &&  \
    rm -r /tmp/catkin_ws

# Create Catkin workspace 
RUN mkdir -p /underlay_ws/src \
 && cd /underlay_ws/src \
 && source /opt/ros/noetic/setup.bash \
 && catkin_init_workspace \
 && cd /underlay_ws \
 && source /opt/ros/noetic/setup.bash \
 && catkin_make

RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "source /underlay_ws/devel/setup.bash" >> ~/.bashrc


# Add user and group
# ARG {VARIABLE} are defined in .env
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD
ARG WORKSPACE_DIR
RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}

# Terminator Config
RUN mkdir -p /home/${USER_NAME}/.config/terminator/
COPY assets/terminator_config /home/${USER_NAME}/.config/terminator/config 
RUN sudo chmod 777 /home/${USER_NAME}/.config/terminator/config
COPY assets/entrypoint.sh /tmp/entrypoint.sh

# .bashrc
RUN echo "source /opt/ros/noetic/setup.bash" >> /home/${USER_NAME}/.bashrc
#RUN echo "# disable to display dbind-WARNING" >> /home/${USER_NAME}/.bashrc
#RUN echo "export NO_AT_BRIDGE=1" >> /home/${USER_NAME}/.bashrc

WORKDIR ${WORKSPACE_DIR}
ENTRYPOINT ["/tmp/entrypoint.sh"]
#CMD ["/bin/bash"]
CMD ["terminator"]

