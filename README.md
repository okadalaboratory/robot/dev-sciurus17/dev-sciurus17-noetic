# Sciurus17をROS1 Noetic Dockerで動かす
Sciurus17をdockerから簡単に使いたい。

## 工事中
- 2023.12.25
Sciurus17の開発環境をDockerで整備する。

Intel系のCPUでの動作を確認しています。

Windowsだとx64、Ubuntuだとand64系です。
手元のマシンだと、Ubuntu22.04, Ubuntu20.04、Windows11（WSL2,Ubuntu20.04）などでの動作を確認してます。


## 事前の準備
お使いのコンピュータにDockerとDocker composeをインストールしてください。
下記の環境での動作を確認しています。

- Ubuntu 22.04.3 LTS
    - Docker version 24.0.7, build afdd53b
    - Docker Copmose version v2.21.0
- Ubuntu 20.04 LTS
    - Docker version 
    - Docker Copmose version

- MacBook Air M2 macOS Sonoma 14.0
    - Docker Desktop for Mac version 20.10.17, build 100c701
    - Docker Compose version v2.10.2

- Windows11 Education
    - 
    -

### Docker と Docker Compose　をインストールする
### NVIDIA ドライバー と NVIDIA container toolkitをインストールする
CPU版を使う場合は不要です

## 簡単に試してみる
Dockerイメージをクラウド上のDocker Hubからダウンロードすることで簡単に試すことができます。イメージのダウンロードには時間がかかるので気長に待ってください。

ダウンロードが終了したらdockerコマンドでdockerコンテナを起動できます。

(工事中)
```
$ cd ~
$ docker pull okdhryk/sciurus-nvidia
$ docker run --rm -it --net host -e DISPLAY=$DISPLAY -e NO_AT_BRIDGE=1 okdhryk/sciurus-nvidia
```


### 開発環境をインストールする　（このリポジトリ)
下記の通り、リポジトリをクローンします。
```
$ cd ~
$ git clone https://gitlab.com/okadalaboratory/robot/dev-sciurus17/dev-sciurus17-noetic.git
$ cd dev-sciurus17-noetic
$ docker compose build
```
イメージがokdhryk/sciurus-nvidiaという名前で作成されます。
イメージ名を変更したい場合は、docker-compose.ymlを編集してください。


## 初めて実行する前の大事な注意
Dockerコンテナで行った、ディレクトリの作成やファイルの編集、パッケージのインストールなどはDockerイメージには反映されず、コンテナが破棄された時点で消えてしまいます。
オブジェクト指向で言うところのクラス（Dockerイメージ）とインスタンス（Dockerコンテナ）の関係です。

そこで、ホストコンピュータのディレクトリをコンテナにマウントし共有します。
このようにすれば、相互にマウントしたディレクトリ内での編集作業が消えることはありません。


### ホストコンピュータとコンテナ間で共有するディレクトリの作成
コンテナを初めて起動するする前に、事前の準備として下記の通り、ホストコンピュータにディレクトリを作成しておいてください。
```
$ cd ~
$ mkdir share
```
ここでは"share"という名前のディレクトリを作成しています。
共有するディレクトリの名前は .envファイルで下記のように指定しています。"share"以外の名前にしたい場合は.envファイルを編集してください。
```
WORKSPACE_DIR=/home/roboworks/share
```

### 事前に共有ディレクトリを作成しなかった場合
事前に共有ディレクトリを作成しなかった場合、Dockerシステムは共有ディレクトリを自動的に作成します。
ただし、自動的に作成された共有ディレクトリはroot権限で作成されるため、書き込みが自由にできないディレクトリになってしまいます。
そのような場合はコンテナを停止した後に、ホストコンピュータで下記のように共有したいディレクトリのユーザとグループを変更してください。
```
$ cd ~
$ ls -al share
合計 8
drwxr-xr-x  2 root   roor     4096 12月 31 23:29 .
drwxr-x--- 24 ユーザ名　グループ名 4096 12月 31 23:29 ..
```

```
$ sudo chown ユーザ名 share/
$ sudo chgrp グループ名 share/
```
共有ディレクトリのユーザ名とグループ名が変更されているか確認します。
```
$ ls -al share
合計 8
drwxr-xr-x  2 ユーザ名 グループ名 4096 12月 31 23:31 .
drwxr-x--- 24 ユーザ名 グループ名 4096 12月 31 23:31 ..
```

## コンテナの実行
下記のコマンドでコンテナを起動します
```
$ cd ~/dev-sciurus17-noetic
$ docker compose up
```
ホストコンピュータの別の端末から起動中のコンテナに入るには下記のコマンドを実行します。
```
$ cd ~/dev-sciurus17-noetic
$ docker compose exec sciurus17-noetic /bin/bash
```

## シミュレータを動かす
```
roslaunch sciurus17_gazebo sciurus17_with_table.launch

# rvizを使用しない場合
roslaunch sciurus17_gazebo sciurus17_with_table.launch use_rviz:=false
```


## 実機を動かす
実機で動作を確認する場合、制御信号のケーブルを接続し取り扱い説明書に従いモータパワーをONにした状態で次のコマンドを実行します。
```
roslaunch sciurus17_bringup sciurus17_bringup.launch
```

### カメラを使用しない場合
次のようにオプションを指定するとカメラを使用しない状態で起動します。
```
roslaunch sciurus17_bringup sciurus17_bringup.launch use_head_camera:=false use_chest_camera:=false
```

### rvizを使用しない場合
次のようにオプションを指定するとrvizによる画面表示を使用しない状態で起動します。画面表示を省略することで制御用パソコンの負荷を下げることができます。
```
roslaunch sciurus17_bringup sciurus17_bringup.launch use_rviz:=false
```

## 参考

## メモ
macOSの場合は
```
docker exec -e DISPLAY=host.docker.internal:0 -it sciurus-notic bash
```




![screenshot](https://gitlab.com/okadalaboratory/robot/dev-sciurus17/dev-sciurus17-noetic/-/raw/images/68747470733a2f2f72742d6e65742e6a702f77702d636f6e74656e742f75706c6f6164732f323032302f30352f67617a65626f5f7069636b5f616e645f706c6163655f72696768742e676966.gif)




